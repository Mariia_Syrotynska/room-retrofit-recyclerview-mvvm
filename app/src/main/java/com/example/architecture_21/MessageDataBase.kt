package com.example.architecture_21

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

/**
 * in this class we create DB
 */
@Database(entities = [MessageEntity::class], version = 1)
abstract class MessageDataBase : RoomDatabase() {
    abstract val messageDao: MessageDao

    companion object {
        private var instance: MessageDataBase? = null

        @Synchronized
        fun getInstance(context: Context): MessageDataBase? {
            if (instance == null) {
                instance = Room.databaseBuilder(
                    context.applicationContext,
                    MessageDataBase::class.java, "message_database")
                    .fallbackToDestructiveMigration()
                    .build()
            }
            return instance
        }
    }
}