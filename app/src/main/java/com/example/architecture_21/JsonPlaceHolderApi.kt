package com.example.architecture_21

import retrofit2.http.GET
import retrofit2.Call

/**
 * this class implements requests to the server
 */
interface JsonPlaceHolderApi {
    //get messages from server
    @get:GET("posts")
    val messages: Call<List<MessageApiModel>>
}