package com.example.architecture_21

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * this class implements requests to the DB
 */
@Dao
interface MessageDao {
    //insert data to the DB
    @Insert(onConflict = OnConflictStrategy.REPLACE)
   suspend fun insertMessages(messageEntity: List<MessageEntity>)

    //get data from DB
    @Query("SELECT * FROM MessageEntity")
    fun getMessagesLiveData(): LiveData<List<MessageEntity>>

    //find messages in DB by user ID
    @Query("SELECT * FROM MessageEntity WHERE userId LIKE :searchingUserId")
    fun findMessageByUserId(searchingUserId: String): LiveData<List<MessageEntity>>
}

