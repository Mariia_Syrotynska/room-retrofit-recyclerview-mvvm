package com.example.architecture_21

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.LiveData
import com.example.architecture_21.MessageDataBase.Companion.getInstance
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory
import java.util.ArrayList

/**
 * Repository class
 * get data from server and insert there to local data base
 * implementation all functions which help us work with messages from data base
 */
class MessageRepository(private val application: Application) {
    private val jsonPlaceHolderApi: JsonPlaceHolderApi
    private val messageDao: MessageDao

    // get data from DB
    val message: LiveData<List<MessageEntity>>

    //get access to the server
    private val server: JsonPlaceHolderApi
        get() {
            val retrofit = Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create(JsonPlaceHolderApi::class.java)
        }

    /**
     * this method get messages from server and insert there to data base
     */
    suspend fun messagesFromServerAndInsertToDB() {
        //get messages from server
        val response = jsonPlaceHolderApi.messages.awaitResponse()
        if (response.isSuccessful) {
            val messageApiModels = response.body()!!
            val listMessageEntity =
                converterMessagesApiModelToMessagesEntity(messageApiModels)
            //set messages to DB
            messageDao.insertMessages(listMessageEntity)
        } else {
            withContext(Dispatchers.Main) {
                Toast.makeText(application, "Code: " + response.code(), Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

    /**
     * this method convert messages from server to messages from DB
     *
     * @param messageApiModelList - messages form server
     * @return list of messages to data base
     */
    private fun converterMessagesApiModelToMessagesEntity(messageApiModelList: List<MessageApiModel>): List<MessageEntity> {
        val listMessageEntity: MutableList<MessageEntity> = ArrayList()
        for (messages in messageApiModelList) {
            val messageEntity = MessageEntity(
                userId = messages.userId,
                id = messages.id,
                title = messages.title,
                text = messages.text
            )
            //insert converted message to DB
            listMessageEntity.add(messageEntity)
        }
        return listMessageEntity
    }

    /**
     * get messages by specified User Id
     *
     * @param searchingUserId number of user ID which we search
     * @return list of messages with searching ID
     */
    fun getMessagesBySpecifiedUserId(searchingUserId: String): LiveData<List<MessageEntity>> {
        return messageDao.findMessageByUserId(searchingUserId)
    }

    init {
        //get access to DB
        val dataBase = getInstance(application)
        messageDao = dataBase!!.messageDao
        message = messageDao.getMessagesLiveData()
        //get access to server
        jsonPlaceHolderApi = server
    }
}