package com.example.architecture_21

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_search_user_id.*

class SearchUserId : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_user_id)

        searchUserIdButton.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        val intent = Intent()
        //press search User ID Button
        if (v.id == R.id.searchUserIdButton) {
            // get user ID
            val userId = userID
            intent.putExtra("user ID", userId)
            //sent data to main activity
            setResult(RESULT_OK, intent)
            finish()
        }
    }

    /**
     * this method get data from edit text
     *
     * @return user ID
     */
    private val userID: String
        get() = searchUserIdEditText.text.toString()
}