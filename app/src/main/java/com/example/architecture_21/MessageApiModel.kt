package com.example.architecture_21

import com.google.gson.annotations.SerializedName

/**
 * this class describes messages from the server
 */
data class MessageApiModel(
    val userId: Int,
    val id: Int,
    val title: String,

    @SerializedName("body")
    val text: String
)