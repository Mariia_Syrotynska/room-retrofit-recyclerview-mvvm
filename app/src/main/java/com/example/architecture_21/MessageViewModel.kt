package com.example.architecture_21

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * View Model class
 * implementation all functions from repository
 */
class MessageViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: MessageRepository = MessageRepository(application)

    //get all messages from DB
    val message: LiveData<List<MessageEntity>> = repository.message

    /**
     * update message from DB
     */
    fun updateMessages() {
        viewModelScope.launch(Dispatchers.IO) {
            repository.messagesFromServerAndInsertToDB()
        }
    }

    /**
     * get messages by specified User Id
     *
     * @param searchingUserId number of user ID which we search
     * @return list of messages with searching ID
     */
    fun getMessagesBySpecifiedUserId(searchingUserId: String): LiveData<List<MessageEntity>> {
        return repository.getMessagesBySpecifiedUserId(searchingUserId)
    }

}