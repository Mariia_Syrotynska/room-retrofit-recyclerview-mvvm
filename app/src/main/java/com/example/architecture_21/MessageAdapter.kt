package com.example.architecture_21

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.architecture_21.MessageAdapter.MessageHolder
import java.util.*

/**
 * this class implements Recycler View Adapter
 */
class MessageAdapter : RecyclerView.Adapter<MessageHolder>() {
    private var messages: List<MessageEntity> = ArrayList()

    /**
     * this method gets data which should to show in Recycler View
     *
     * @param messages - list of messages from DB
     */
    fun setMessages(messages: List<MessageEntity>) {
        this.messages = messages
        notifyDataSetChanged()
    }

    /**
     * this method finds a layout that shows what the holder item will look like
     * @param parent
     * @param viewType
     * @return - holder
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.message_item, parent, false)
        return MessageHolder(view)
    }

    /**
     * this method calls every time when user scroll the list of messages and refresh information on screen
     *
     * @param holder   - View Holder
     * @param position - where will be insert View Holder
     */
    override fun onBindViewHolder(holder: MessageHolder, position: Int) {
        val message = messages[position]
        holder.bind(message)
    }

    /**
     * this method get sum of items
     *
     * @return - amount of items
     */
    override fun getItemCount(): Int {
        return if (messages == null) 0 else messages.size
    }

    /**
     * this class implements how to should look the View Holder
     */
    class MessageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //create and find views from item layout
        private val idTextView: TextView = itemView.findViewById(R.id.idTextView)
        private val userIdTextView: TextView = itemView.findViewById(R.id.userIdTextView)
        private val titleTextView: TextView = itemView.findViewById(R.id.titleTextView)
        private val messageTextView: TextView = itemView.findViewById(R.id.messageTextView)

        /**
         * this method set text in views from item layout
         *
         * @param message - list messages from DB
         */
        fun bind(message: MessageEntity) {
            //set data to text views
            idTextView.text = "ID: " + message.id
            userIdTextView.text = "User ID: " + message.userId
            titleTextView.text = "Title: " + message.title
            messageTextView.text = "Text: " + message.text
        }
    }
}