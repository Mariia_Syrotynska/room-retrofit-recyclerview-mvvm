package com.example.architecture_21

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * This class describes messages from the DB
 */
@Entity
data class MessageEntity(
    @PrimaryKey
    var id: Int,
    var userId: Int,
    var title: String,
    var text: String
)