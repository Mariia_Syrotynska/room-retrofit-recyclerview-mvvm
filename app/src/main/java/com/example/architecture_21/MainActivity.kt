package com.example.architecture_21

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var messageViewModel: MessageViewModel
    private lateinit var adapter: MessageAdapter
    private val REQUESTCODE = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //subscribe to buttons
        updateButton.setOnClickListener(this)
        chooseUserIdButton.setOnClickListener(this)

        //implementation adapter
        adapter = initAdapter()

        //implementation view model
        messageViewModel = initViewModel()

        //shows the message
        observeMessage()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.updateButton -> messageViewModel.updateMessages()
            R.id.chooseUserIdButton -> {
                val intent = Intent(this, SearchUserId::class.java)
                startActivityForResult(intent, REQUESTCODE)
            }
        }
    }

    /**
     * this method initializes adapter and recyclerview
     *
     * @return adapter
     */
    private fun initAdapter(): MessageAdapter {
        val adapter = MessageAdapter()
        val recyclerView = findViewById<RecyclerView>(R.id.myRecyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = adapter
        return adapter
    }

    /**
     * this method initializes view model
     *
     * @return view model
     */
    private fun initViewModel(): MessageViewModel {
        messageViewModel = ViewModelProvider(
            this,
            ViewModelProvider.AndroidViewModelFactory.getInstance(this.application)
        ).get(
            MessageViewModel::class.java
        )
        return messageViewModel
    }

    /**
     * this method shows the message
     */
    private fun observeMessage() {
        messageViewModel.message.observe(this, { messages ->
            //update RecyclerView
            adapter.setMessages(messages)
        })
    }

    /**
     * this method get results from search user ID activity
     *
     * @param requestCode - request code
     * @param resultCode  - result code
     * @param data        - activity
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // set value of requestCode and resultCode
        super.onActivityResult(requestCode, resultCode, data)
        // if requestCode and resultCode is OK
        if (resultCode == RESULT_OK && requestCode == REQUESTCODE) {
            observeFoundMessages(data!!)
        } else {
            // if requestCode and resultCode is not OK
            Toast.makeText(this, "Wrong result", Toast.LENGTH_SHORT).show()
        }
    }

    /**
     * this method shows the found messages by User ID
     *
     * @param data        - activity
     */
    private fun observeFoundMessages(data: Intent?) {
        //get result from search user ID layout
        val searchingUserId = data?.getStringExtra("user ID")
        //get result from DB
        messageViewModel.getMessagesBySpecifiedUserId(searchingUserId!!)
            .observe(this, { messageEntities -> adapter.setMessages(messageEntities) })
    }
}